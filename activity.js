// Inserting/creating collections in s29 activity database
db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);

// Finding letter S/s in collection's field first name  or letter D/d in their last name and hiding _id field
db.users.find({ $or: [{ firstName: {$regex: "S", $options: "i"} }, { lastName: {$regex: "D", $options: "i"}}] 
        },
            {
                firstName: 1,
                lastName: 1,
                _id: 0
            }
    );


// Finding users from HR department with age is greater than or equal to 70
db.users.find({ $and: [{ department: "HR"}, {age: {$gte: 70}}]});


// Users with letter in their first name and has an age of less than or equal to 30
db.users.find({ $and: [{ firstName: {$regex: "E", $options: "i"} }, {age: {$lte: 30} } ]});









